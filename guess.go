/* Guess mimics a villain who has a
random number for the user to guess*/
package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	seconds := time.Now().Unix()
	rand.Seed(seconds)
	target := rand.Intn(100) + 1
	fmt.Println("I've chosen a random number between 1 and 100")
	fmt.Println("Can you guess it?")
	//fmt.Println(target)     for debuging

	reader := bufio.NewReader(os.Stdin)

	success := false
	for guesses := 0; guesses < 10; guesses++ {
		fmt.Println("you have", 10-guesses, "guesses left.")

		fmt.Print("Make a guess: ")
		input, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}
		input = strings.TrimSpace(input)
		guess, err := strconv.Atoi(input)
		if err != nil {
			log.Fatal(err)
		}

		if guess < target {
			fmt.Println("Haha Your guess was TOO low")
		} else if guess > target {
			fmt.Println("Muahaha your guess was TOO high")
		} else {
			success = true
			fmt.Println("darn it, your guess was right and beat me")
			break
		}
	}
	if !success {
		fmt.Println("hehehe I have beten you. My number is: ", target)
	}
}
